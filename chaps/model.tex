\section{Battery Life Prediction}

As mentioned in Section~\ref{sec:problem}, the task of battery life prediction is formulated as a regression problem. Therefore, the battery life prediction system is essentially centered on a regression model, which is trained offline and generates predictions in the run time based on the given queries and the features extracted from the sessions. To be specific, we train prediction models based on comprehensive features from smartphone usage data, through multiple machine learning methods. In this section, we describe the overview of the prediction system, the machine learning methods, and the features used by our model in details.

\subsection{System Overview}

The general workflow of our battery life prediction system is illustrated in Figure~\ref{fig:overview}. %In general, the battery life prediction model is trained in an offline fashion. Therefore, the model is trained based on a fixed data set in advance, and responds to new query instantly. 
The whole pipeline consists of four major phases:

\begin{itemize}
	\item \textbf{Data Collection.} In the first phase, the system collects a group of sessions with queries and targets as the training data. It also collects a comprehensive set of usage data related to these sessions for feature extraction.
	\item \textbf{Battery Life Extraction \& Feature Extraction.} For each session, query, and target, the system first calculates its actual battery life. Then, multiple features are extracted from the usage data, which represent various aspects of the user's usage behavior and the device's status at the query, earlier in the session, and in history. %We will introduce these features in this section later. After all the features are extracted, the model combines 
	All the features together are represented as a high-dimensional vector. We call this vector ``feature vector'' in the rest of this paper. 
	\item \textbf{Model Training.} The system then trains a group of regression models to derive the correlations between the features and the battery life. In this work, we select several regression models such as linear regression and tree-based machine learning models. These models can capture both linear and non-linear correlations between the features and the battery life. This phase eventually generates a prediction model for battery life.
	\item \textbf{Prediction.} Finally, given a new session, a new query, and a user-defined target, the system extracts the feature vector of this new example and then predicts the remaining battery life using the trained machine learning models.
\end{itemize}

\input{figures/approach-overview.tex}

\subsection{Machine Learning Models}

%To make our model more predictive, w
We explore a group of standard, representative machine learning models, including the \textit{Linear Regression}, the \textit{Random Forest Regression} (RF)~\cite{breiman2001random}, the \textit{Gradient Boosted Regression Tree} (GBRT)~\cite{friedman2002stochastic}, and the \textit{Extreme Gradient Boosting} (XGBoost)~\cite{friedman2001greedy}. These models can capture both linear and non-linear correlations between the features to the prediction outcome\footnote{We use the XGBoost package~\cite{chen2016xgboost} for XGBoost, and scikit-learn package~\cite{pedregosa2011scikit} for Linear Regression, RF, and GBRT. Both packages are implemented in Python. We use default parameters when training all the models. For example, \texttt{n\_estimators=100} and \texttt{learning\_rate=0.1} for GBRT and XGBoost, while \texttt{max\_features=''auto''} for RF.}. Besides these four models, we have also considered two additional models: \textit{Support Vector Regression} and \textit{Decision Tree Regression}. Their performance is inferior to the aforementioned models. For simplicity, we will focus on the four models in this paper.

A smartphone is actually a computer system that contains numerous software and hardware components. The battery consuming rate is most likely to be a complex, nonlinear combinations of these factors. Therefore, we think that it is useful to capture inner correlations among system features and non-linear correlations between the system and behavior features and battery life. That is why we adopt the tree-based machine learning models, and \textbf{we hypothesize that tree-based models can outperform the linear regression model}. %Generally, the three tree-based models are state-of-the-art models that are all widely used. 
We will compare their performance when evaluating the prediction results.

\subsection{Feature Categorization}
\label{sec:feature-cate}

The proposed system aims to use a comprehensive set of features to predict battery life. %In this subsection, we will introduce the conceptual features used in the model and explain the intuition of why we select them.
Conceptually, these features can be divided into the following categories: 

The most straightforward feature that predicts battery life is the difference between the battery level at query time and the target battery level, or $b(t_q) - b_n$. Generally, if this gap is larger, the battery is likely to last longer. Many existing practices estimate the battery life purely based on this feature. %No feature could beat battery gap, and all features could work only when they are used together with battery gap. Therefore, battery gap definitely should be considered as a feature. 

Beyond the difference between the battery level at query time and the target battery level, we expect that the status of the device at the time of query ($t_q$) should also contribute to the prediction. This is evidenced in Section~\ref{sec:descriptive} (Figure~\ref{fig:descriptive-current}). Beyond device status like the battery level at query time, other information such as the hour of the day or the apps currently running may also have a predictive power. For example, map apps are frequently used during rush hours and drain the battery fast. We call this kind of features \textbf{``query-time features''}. %Query-time features describe the context when the user makes the query, which could be highly related to the battery life in the future. For example, if the user makes the query at rush hour, we may infer that the battery would not last long since users tend to use smartphones more heavily on the way. From this point of view, we could understand that query-time features might be helpful. 

Query-time features are far from sufficient. They only describe one static snapshot of the device, while battery consumption is a notion of change. Information of the time period before the query occurs indicates the trend of battery consumption, thus can be quite predictive of the battery life (see Figure~\ref{fig:descriptive-before}). If a feature summarizes the system statuses and behavior in the same session and before the query, we call it a \textbf{``session feature''}. %We have the hypothesis that users' usage will usually be stable, which means their usage behavior in the future should have common patterns that appeared in the past. In this case, the usage behavior and system status before the query time can somehow reflect the usage pattern in the future, so it can help predicting the battery life. 

Query-time features and session features give a good summary of short-term patterns of device usage, but they are not able to represent long-term usage patterns. Would the latter be predictive to the remaining battery life of the current session? Presumably, every user has their own habits, so how they use the device in the future may be reflected by how they use the device in the past (see Figure~\ref{fig:descriptive-history}). Therefore, we also explore features that describe a user's usage behavior in the past sessions. We call these features \textbf{``user history features''}. %By investigating user-specific features, we could know users' usage habit, which may be helpful for battery life prediction.

Now we have introduced three categories of features according to the time point or period they are associated to. In each of the categories, we may also partition the features from another perspective: the type of signals it summarizes. In our model, we categorize features into three categories: \textbf{contexts}, \textbf{usage behavior}, and \textbf{device statuses}. Contexts include contextual information of the query and its session, for example, the time when the session begins. Usage behavior demonstrates how the user use the device, for example, the apps being used when the query occurs. Device statuses contains various OS status and hardware status, for example, readings of the sensors of the device. These three kinds of features could cover most aspects of the information related to a query. In conclusion, the battery life prediction models are built upon a comprehensive set of features. 

Note that at this point, the definition of features is very general and can be applied to any device models and all major operation systems, as long as the periodical readings of battery levels, sensor signals, and historical user behaviors are available. Given the general categories of features and the standard machine learning models, the proposed battery life prediction method is generalizable to any modern device models. In the next section, we instantiate the proposed method to the Sherlock data set and empirically evaluate its effectiveness. 

















